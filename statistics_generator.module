<?php

/**
 * Implementation of hook_perm().
 */
function statistics_generator_perm() {
  return array('access stats gen', 'administer stats gen');
}

/**
 * Implementation of hook_menu().
 */ 
function statistics_generator_menu() {
  $items['views-report'] = array(
    'title'            => 'Report',
    'page callback'    => 'statistics_generator_report_page',
    'access arguments' => array('access contents'),
    'type'             => MENU_CALLBACK,
  );
  $items['admin/content/stats-gen'] = array(
    'title' => 'Statistic Generator',
    'access arguments' => array('access stats gen'),
    'page callback' => 'statistics_generator_list_stats',
    'description' => 'Page list available stats',
    'type' => MENU_NORMAL_ITEM
  );
  $items['admin/content/stats-gen/list'] = array(
    'title' => 'List',
    'access arguments' => array('access stats gen'),
    'page callback' => 'statistics_generator_list_stats',
    'weight' => -1,
    'type' => MENU_DEFAULT_LOCAL_TASK
  );
  $items['admin/content/stats-gen/add'] = array(
    'title' => 'Add',
    'access arguments' => array('administer stats gen'),
    'page arguments' => array('statistics_generator_stats_form'),
    'page callback' => 'drupal_get_form',
    'description' => 'Page list available stats',
    'type' => MENU_LOCAL_TASK
  );  
  $items['statistics-generator/%/view'] = array(
    'title' => 'Statistic Generator',
    'access arguments' => array('access stats gen'),
    'page arguments' => array(1),
    'page callback' => 'statistics_generator_view',
    'description' => 'Page list available stats',
    'type' => MENU_NORMAL_ITEM
  );
  $items['admin/content/stats-gen/%/edit'] = array(
    'title' => 'Statistic Generator',
    'access arguments' => array('administer stats gen'),
    'page arguments' => array('statistics_generator_stats_form', 3),
    'page callback' => 'drupal_get_form',
    'description' => 'Page list available stats',
    'type' => MENU_NORMAL_ITEM
  );
  $items['admin/content/stats-gen/%/add_item'] = array(
    'title' => 'Statistic Generator add item to group',
    'access arguments' => array('administer stats gen'),
    'page arguments' => array('statistics_generator_add_item_group_form', 3),
    'page callback' => 'drupal_get_form',
    'description' => 'Add item to a statistics group',
    'type' => MENU_CALLBACK,
  );
  $items['admin/content/stats-gen/%/delete'] = array(
    'title' => 'Statistic Generator',
    'access arguments' => array('administer stats gen'),
    'page arguments' => array('statistics_generator_stats_delete_form', 3),
    'page callback' => 'drupal_get_form',
    'description' => 'Page list available stats',
    'type' => MENU_NORMAL_ITEM
  );
  $items['admin/content/stats-gen/remove/item/%'] = array(
    'title' => 'Remove view',
    'access arguments' => array('administer stats gen'),
    'page arguments' => array('statistics_generator_stats_remove_item_form', 5),
    'page callback' => 'drupal_get_form',
    'description' => 'Page list available stats',
    'type' => MENU_NORMAL_ITEM
  );  
   
  $items['statistics-generator/%/compare'] = array(
    'title' => 'Statistics comparation',
    'access arguments' => array('access stats gen'),
    'description' => 'Comparation of statistics results based in the filters criteria.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_statistics_generator_compare_filter', 1),
    'type' => MENU_CALLBACK,
  );
  $items['statistics-generator/%/compare/view'] = array(
    'title' => 'Statistic comparation results',
    'access arguments' => array('access stats gen'),
    'page arguments' => array(1),
    'page callback' => 'statistics_generator_compare_page',
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
 * Helper form function that implement the hierarchical_select form element
 * to be used as a localization filter using taxonomies
 */
function _statistics_generator_filter_taxonomy($sgid, $delta, $args = NULL) {
  $sgid_delta = $sgid . '_' . $delta;
  $form['taxonomy'.$sgid_delta] = array(
    '#type' => 'hierarchical_select',
    '#title' => $args['title'],
    '#size' => 1,
    '#config' => array(
    'module' => 'hs_taxonomy',
    'params' => array(
      'vid' => $args['vid'],
    ),
    'save_lineage'    => 0,
    'enforce_deepest' => 0,
    'resizable'       => 0,
    'level_labels' => array(
      'status' => 1,
      'labels' => $args['labels'],
      ),
    'animation_delay'    => 400,
    'special_items'      => array(),
    'render_flat_select' => 1,
    'path'               => 'hierarchical_select_json',
    ),
    '#default_value' => $_GET['taxonomy'.$sgid_delta]['flat_select'],
    '#required' => false,
  ); 

  return $form;
}

/**  
 * Helper function to generate a form item with a date widget to works as the
 * visible filter, the input of this form will be mapped to a view filter
 */
function _statistics_generator_filter_date($sgid, $delta, $args = NULL) {
  $sgid_delta = $sgid . '_' . $delta;
  $default_value = array();
  foreach (array('date_min', 'date_max') as $key) {
    if (date_is_valid($_GET[$key.$sgid_delta], DATE_ARRAY)) {
      $default_value[$key.$sgid_delta] = date_fuzzy_datetime(date_convert($_GET[$key.$sgid_delta], DATE_ARRAY, DATE_DATETIME));
    }
  }

  if($default_value['date_min'.$sgid_delta] > $default_value['date_max'.$sgid_delta]) {
    form_set_error('date_min', t('The To date must be greater than the From date.'));
  }

  $form['date_min'.$sgid_delta] = array(
    '#type' => 'date_select',
    '#date_format' => 'm/d/Y',
    '#title' => 'Date start',
    '#required' => false,
    '#default_value' => $default_value['date_min'.$sgid_delta],
  );

  $form['date_max'.$sgid_delta] = array(
    '#type' => 'date_select',
    '#date_format' => 'm/d/Y',
    '#title' => 'Date end',
    '#required' => false,
    '#default_value' => $default_value['date_max'.$sgid_delta],
  );
   
  return $form;
}

/**  
 * Create the custom filters form invoking the hook gen filters and building 
 * the form that is rendered in the current statistics group
 */
function _statistics_generator_filter_view($form_state, $sgid, $delta = 0) {
  $form = array();

  // get the custom  form filters from hook
  $filters_form = _statistics_generator_add_filters_hooks($sgid, $delta);
  $form = array_merge($form, $filters_form);

  if (isset($form['empty'])) {
    return $form;
  }

  $form['#action'] = url("statistics-generator/$sgid/view");
  $form['filter'] = array(
    '#type' => 'submit',
    '#value' => 'filter',
  );

  $form['#validate'] = '_statistics_generator_filter_view_form_validate';
  $form['#method'] = 'get';
  return $form;
}

/**  
 * Helper function that invoke the hook_statistics_generator_filters 
 * and build the custom filters forms 
 *  
 * @params int $sgid 
 *   statistics group ID 
 * @params int $delta 
 *   delta int value to differentiate similar form filter in compare form 
 * @return array form filters
 */
function _statistics_generator_add_filters_hooks($sgid, $delta) {
  $form = array();
  $stat = statistics_generator_load_stats($sgid);
  $filter_item = module_invoke_all('statistics_generator_filters', $stat);

  foreach ($filter_item as $key => $filter) {
    if (function_exists($filter['callback'])) {
      $form_filter = $filter['callback']($sgid, $delta.$key, $filter['args']);
      $form = array_merge($form, $form_filter);
    } 
  }

  if (empty($form)) {
    $form['empty'] = array(
      '#value' => t('There are not custom filters available for this statistics group'),
    );
  }

  return $form;
}

function _statistics_generator_filter_get_form_filters_ids($sgid, $delta = 0) {
  $stat = statistics_generator_load_stats($sgid);
  $filters = module_invoke_all('statistics_generator_filters', $stat);
  $filters_map_views = array();

  foreach ($filters as $key => $filter) {
    if (function_exists($filter['callback'])) {
      $form_filter = $filter['callback']($sgid, $delta.$key, $filter['args']);
      $form_ids = array_keys($form_filter);
      $filter_map = array();

      foreach($form_ids as &$form_id) { 
        if (isset($form_filter[$form_id]['#type'])) {
          switch ($form_filter[$form_id]['#type']) {
            case 'hierarchical_select' :
              $form_id = $form_id . '#hierarchical_select#selects';
              break;
          }
        }
      }

      $filter_map = array_fill_keys($filter['apply_view_filters'], $form_ids);
      if (!empty($filter_map)) {
        $filters_map_views = array_merge($filters_map_views, $filter_map);
      }
    }
  }
  return $filters_map_views;
}
  
/**
 * Retrieve the group stats views and apply the mapping of custom filters to the
 * views filters and print the output of views and the custom filters forms
 **/
function statistics_generator_view_page($sgid, $delta = 0, $type = 'single', $empty_views = array()) {
  $output = '';
  $filters_map_views = _statistics_generator_filter_get_form_filters_ids($sgid, $delta);

  $stat = statistics_generator_load_stats($sgid);
  if (!$stat) {
    return $output;
  }

  foreach ($stat['items'] as $item) {
    // if this view was detected as empty in any of the sides, then ignore
    // doesn't make sense to compare a empty charts
    if (in_array($item['item'], $empty_views)) {
      continue;
    }

    $view = _statistics_generator_view_map_filters($item['item'], $filters_map_views);
    if ($type == 'single') {
      $display_id = $item['display_id'];
    }
    else {
      $display_id = $item['compare_display_id'];
    }
 
    $output .= $view->render($display_id);  
  }

  return $output;
}

/**  
 * Detect the empty views for a specific group and delta filters 
 *  
 * @param int $sgid 
 *   the ID of the statistics group 
 * @param int $delta 
 *   the delta ID of input filters 
 * @return array
 *   names of the founded empty views
 */
function _statistics_generator_ignore_items($sgid, $delta = 0) {
  $stat = statistics_generator_load_stats($sgid);
  $filters_map_views = _statistics_generator_filter_get_form_filters_ids($sgid, $delta);
  $empty_views = array();

  if (!$stat) {
    return $output;
  }

  foreach ($stat['items'] as $item) {
    $view = _statistics_generator_view_map_filters($item['item'], $filters_map_views);
    $view->execute();
    if (empty($view->result)) {
      $empty_views[]= $view->name;
    }
  }
  return $empty_views;  
}  

function _statistics_generator_view_map_filters($view_name, $filters_map_views) {
  $view = views_get_view($view_name); 
  $filters =& $view->display['default']->display_options['filters'];
  foreach ($filters as $name => &$filter) {
    // to map the custom filters we use the name of the view + "_" and name of the filter
    $view_filter_id = $view->name . '_' . $name;
    $view_filter_id_delta = '*_' . $name;
    $view_filter_id_choice = NULL;

    if (isset($filters_map_views[$view_filter_id])) {
      $view_filter_id_choice = $filters_map_views[$view_filter_id];
    }
    else if (isset($filters_map_views[$view_filter_id_delta])) {
      $view_filter_id_choice = $filters_map_views[$view_filter_id_delta];
    }

    if($view_filter_id_choice) {
      foreach($view_filter_id_choice as $form_element_id) {
        // we use # as separator for the nested levels of form element ids when
        // reusing some form elements them force specific levels in the $_GET array
        $path_form_element_id = '';
        foreach(split('#', $form_element_id) as $key_id) {
          $path_form_element_id .= "[$key_id]";
        }

        // using eval to access directly the path to the values of the custom filters,
        // there are some special paths as with hirarchical_select form elements
        $form_element_value = eval('return ' . '$_GET' . $path_form_element_id . ';');

        // force the value of the filter of the view to our mapped custom filter
        if(!empty($form_element_value)) {

          switch ($filter['operator']) {
            case 'between' :
              // if the filter is of type date_select we convert the date granular array
              // to string date in standard format, that's the way is expected by views
              if($filter['form_type'] == 'date_select') {
                if (date_is_valid($form_element_value, DATE_ARRAY)) {
                  $form_element_value = date_fuzzy_datetime(date_convert($form_element_value, DATE_ARRAY, DATE_DATETIME));

                  // if there are an array of form elements mapped in range this will take
                  // the first to the min key and the last one to the max key
                  if (stristr($path_form_element_id, 'min')) {
                    $filters[$name]['value']['min'] = $form_element_value;
                  }
                  elseif(stristr($path_form_element_id, 'max')) {
                    $filters[$name]['value']['max'] = $form_element_value;
                  }

                }
              }

              break;

            default:
              while($single_value = array_pop($form_element_value)) {
                if (!empty($single_value) && is_numeric($single_value)) {
                  $filters[$name]['value'] = array($single_value);
                  break;
                }
              }

              break;
          }
        }
      }
    }
  }
  return $view;
}  

function statistics_generator_view($sgid) {
  $compare_results = array();
  $empty_views = _statistics_generator_ignore_items($sgid, 0);
  $compare_results = statistics_generator_view_page($sgid, 0, 'single', $empty_views);

  if(empty($compare_results)) {
    return theme('statistics_generator_empty_results');
  }
  return $compare_results; 
}

/**  
 * Process the calls to generate the compare pages based in the filters params 
 * and print the compare panel
 */
function statistics_generator_compare_page($sgid) {
  $compare_results = array();
  $empty_views0 = _statistics_generator_ignore_items($sgid, 0);
  $empty_views1 = _statistics_generator_ignore_items($sgid, 1);
  $empty_views = array_unique(array_merge($empty_views0, $empty_views1));

  $compare_results['filter1'] = statistics_generator_view_page($sgid, 0, 'compare', $empty_views);
  $compare_results['filter2'] = statistics_generator_view_page($sgid, 1, 'compare', $empty_views);

  if(empty($compare_results['filter1']) || empty($compare_results['filter2'])) {
    return theme('statistics_generator_empty_results');
  }
  return theme('statistics_generator_compare_panel', $compare_results);
}

/**  
 * Form that builds a dual filter for a some statistics group for the compare 
 * functionality, dinamically it detects the filters for that group by the hook 
 */
function _statistics_generator_compare_filter($form_state, $sgid) {
  $form = array();
  $form['#action'] = url("statistics-generator/$sgid/compare/view");

  $filters_hooks = array();
  $filters_hooks[] = _statistics_generator_add_filters_hooks($sgid, 0);
  $filters_hooks[] = _statistics_generator_add_filters_hooks($sgid, 1);
 
  foreach ($filters_hooks as $key => $filter_hook) {
    $form['filter' . $key] = array(
      '#type' => 'fieldset',
      '#title' => t('Filter criteria'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['filter'.$key] = array_merge($form['filter'.$key], $filter_hook);
  }
 
  if (isset($filters_hooks[0]['empty']) || isset($filters_hooks[1]['empty'])) {
    return $form;
  }

  $form['filter'] = array(
    '#type' => 'submit',
    '#value' => 'filter',
  );

  $form['#validate'] = '_statistics_generator_filter_view_form_validate';
  $form['#method'] = 'get';
  return $form;
}

/**
 * Form to add/edit statistic title and views items
 */ 
function statistics_generator_stats_form(&$form_state, $sgid = NULL) {
  $stat = statistics_generator_load_stats($sgid);
  $form = array();
  $form['#tree'] = TRUE;

  $form['sgid'] = array(
    '#type' => 'value',
    '#value' => $stat['sgid'],
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $stat['title'],
    '#size' => 50,
    '#maxlength' => 64,
    '#description' => t('Enter the name of the stats'),
    '#required' => TRUE,
  );

  if (isset($stat['sgid'])) {
    $form['machine_name'] = array(
      '#value' => '<p>' . t('Machine name:') . ' ' . $stat['machine_name'] . '</p>',
    );

    $form['add_items'] = array(
      '#value' => l('Add data items', "admin/content/stats-gen/$sgid/add_item")
    );

    foreach ($stat['items'] as $item) {
      $displays_ids = _statistics_generator_get_view_displays_ids($item['item']);

      $form['items'][$item['item_id']]['name'] = array(
        '#value' => $item['item']
      );
      $form['items'][$item['item_id']]['display_id'] = array(
        '#type' => 'select',
        '#title' => t('General display'),
        '#options' => $displays_ids,
        '#value' => $item['display_id'],
      );
      $form['items'][$item['item_id']]['compare_display_id'] = array(
        '#type' => 'select',
        '#title' => t('Compare display'),
        '#options' => $displays_ids, 
        '#value' => $item['compare_display_id'],
      );
      $form['items'][$item['item_id']]['position'] = array(
        '#type' => 'select',
        '#title' => t('Position'),
        '#options' => range(1, count($stat['items'])),
        '#default_value' => $item['position'],
      );
      $form['items'][$item['item_id']]['remove'] = array(
        '#value' => l('Remove', 'admin/content/stats-gen/remove/item/' . $item['item_id'])
      );
    }
  }
  else {
    $form['machine_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Machine name'),
      '#default_value' => empty($stat['machine_name']) ? '' : $stat['machine_name'],
      '#maxlength' => 32,
      '#description' => t('Give the statistics group a machine name to make it exportable with "!features" module.', array('!features' => l('Features', 'http://drupal.org/project/features'))),
      '#required' => TRUE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send changes'),
  );

  // Validate machine name.
  $form['#validate'][] = '_statistics_generator_machine_name_validate';

  return $form;
}

/**
 * Save form handler
 */ 
function statistics_generator_stats_form_submit($form, &$form_state) {
  $values = isset($form_state['values']) ? $form_state['values'] : array();

  // Save to {statistics_generator} table
  if (!empty($values['sgid']) && is_numeric($values['sgid'])) {
    db_query('UPDATE {statistics_generator} SET title = "%s" WHERE sgid = %d', $values['title'], $values['sgid']);
  }
  else {
    db_query('INSERT INTO {statistics_generator} (title, machine_name) values ("%s", "%s")',
    $values['title'], $values['machine_name']);
    $form_state['redirect'] = 'admin/content/stats-gen';
    drupal_set_message(t('New statistic page has been created. Please add views item to create reports.'));
  }
  $values_items = $form_state['clicked_button']['#post']['items'];
  if (isset($values_items)) {
    foreach ($values_items as $item_id => $data) {    
      db_query('UPDATE {statistics_generator_items} SET position = %d, display_id = "%s", compare_display_id = "%s" WHERE item_id = %d', $data['position'], $data['display_id'], $data['compare_display_id'], $item_id);
    }
  }
}

/**  
 * Add item form (data sources) to feed an statistic group 
 * Use the ahah helper module to dinamically add form items 
 * to select the item display to general and compare mode
 */
function statistics_generator_add_item_group_form($form_state, $sgid = NULL) { 
  $stat = statistics_generator_load_stats($sgid);

  // if the statistics group doesn't exists return not found
  if(empty($stat)) {
    drupal_not_found();
    exit;
  }

  $form = array();
  ahah_helper_register($form, $form_state);

  // list of all views names
  $views['select'] = t('Select view');
  foreach (views_get_all_views() as $key => $view) {
    $views[$key] = $view->name;
  }

  if (!isset($form_state['storage']['add_item']['item'])) {
    $default_item = 'select';
  }
  else {
    $default_item =  $form_state['storage']['add_item']['item'];
  }

  $form['add_item'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Statistics views data sources'),
    '#prefix' => '<div id="extras-wrapper">', 
    '#suffix' => '</div>',
    '#tree'   => TRUE,
  );

  $form['add_item']['sgid'] = array(
    '#type' => 'hidden',
    '#value' => $sgid,
  );

  $form['add_item']['item'] = array(
    '#type' => 'select',
    '#title' => t('New item to add'),
    '#options' => $views,
    '#default_value' => $default_item,
    '#ahah' => array(
      'event'   => 'change',
      'path'    => ahah_helper_path(array('add_item')),
      'wrapper' => 'extras-wrapper',
    ),
  );
  // If a item is selected the displays are display
  if ($default_item != 'select') {
    $displays_ids = _statistics_generator_get_view_displays_ids($default_item);
    $form['add_item']['display_id'] = array(
      '#type'      => 'select',
      '#title'     => t('Select general display'),
      '#required'  => TRUE,
      '#options' => $displays_ids,
      '#default_value' => $form_state['storage']['add_item']['display_id'],
    );
    $form['add_item']['compare_display_id'] = array(
      '#type'      => 'select',
      '#title'     => t('Select compare display'),
      '#required'  => TRUE,
      '#options' => $displays_ids,
      '#default_value' => $form_state['storage']['add_item']['compare_display_id'],
    );
  }

  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function statistics_generator_add_item_group_form_submit($form, &$form_state) {
  $values = isset($form_state['values']['add_item']) ? $form_state['values']['add_item'] : array();
  // Save to {statistics_generator_items} table
  if ($values['item'] != 'select') {
    $sql = 'INSERT INTO {statistics_generator_items} (stat_id, item, display_id, compare_display_id, position, status) VALUES (%d, "%s","%s", "%s", %d, %d)';
    db_query($sql, $values['sgid'], $values['item'], $values['display_id'], $values['compare_display_id'], 1, 1);

    drupal_set_message(t("The data item was added to the statics group"));
    drupal_goto("admin/content/stats-gen/{$values['sgid']}/edit");
  }
}

/**  
 * Get a list of displays for a specific view 
 *  
 * @params $view_name
 *   the name of the view 
 * @return array 
 *   list of display names for the view
 */
function _statistics_generator_get_view_displays_ids($view_name) {
  // build the array with views names and display_ids
  $display_ids = array();
  $view = views_get_view($view_name);  

  if ($view) {
    foreach (array_keys($view->display) as $id) {
      $display_ids[$id] = $id;
    }
  }
  return $display_ids;
}

/**
 * Validate machine name.
 */
function _statistics_generator_machine_name_validate($form, &$form_state) {
  if (empty($form_state['values']['machine_name'])) {
    return;
  }
  if (!preg_match('!^[a-z0-9_]+$!', $form_state['values']['machine_name'])) {
    form_set_error('machine_name', t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
  }
  elseif (db_result(db_query("SELECT COUNT(*) FROM {statistics_generator} WHERE sgid <> %d AND
  machine_name = '%s'", $form_state['values']['sgid'], $form_state['values']['machine_name']))) {
    form_set_error('machine_name', t('The machine-readable name has been taken. Please pick another one.'));
  }
}

/**
 * Confirm form to delete a stat
 */
function statistics_generator_stats_delete_form(&$form_state, $sgid) {
  $form['sgid'] = array('#type'  => 'value', '#value' => $sgid);
  return confirm_form($form,
    t('Are you sure you want to delete'),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/stats-gen',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
  return $form;
}

/**
 * Submit function for deleting stat
 */
function statistics_generator_stats_delete_form_submit($form_id, &$form_state) {
  if ($form_state['values']['confirm']) {
    statistics_generator_delete($form_state['values']['sgid']);
    drupal_set_message(t('The statistics page has been deleted.'));
  }
  $form_state['redirect'] = 'admin/content/stats-gen';
}

/**
 * Function deletes statistic page and its associated items.
 */ 
function statistics_generator_delete($sgid) {
  db_query('DELETE FROM {statistics_generator} WHERE sgid = %d', $sgid);
  db_query('DELETE FROM {statistics_generator_items} WHERE stat_id = %d', $sgid);
}

/**
 * Function provide form to removes a view item from statistics page.
 */ 
function statistics_generator_stats_remove_item_form(&$form_state, $item_id) {
  $form['item_id'] = array('#type'  => 'value', '#value' => $item_id);
  return confirm_form($form,
    t('Are you sure you want to delete'),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/stats-gen',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
  return $form;
}

/**
 * Function to removes a view item from statistics page.
 */
function statistics_generator_stats_remove_item_form_submit($form_id, &$form_state) { 
  if ($form_state['values']['confirm']) {
    db_query('DELETE FROM {statistics_generator_items} WHERE item_id = %d', $form_state['values']['item_id']);
  }
  $form_state['redirect'] = 'admin/content/stats-gen';
}

/**
 * Callback listing available statistics pages
 */ 
function statistics_generator_list_stats() {
  $header = array(t('Title'), t('Operations'));
  $rows = array();  
  foreach (statistics_generator_load_stats(NULL, FALSE) as $stat) {
    $op_links['view'] = l('View', 'statistics-generator/' . $stat['sgid'] . '/view');
    $op_links['compare'] = l('Compare', 'statistics-generator/' . $stat['sgid'] . '/compare');
    if (user_access('administer stats gen')) {
      $op_links['edit'] = l('Edit', 'admin/content/stats-gen/' . $stat['sgid'] . '/edit');
      $op_links['delete'] = l('Delete', 'admin/content/stats-gen/' . $stat['sgid'] . '/delete');
    }      
    $rows[] = array($stat['title'], implode(' | ', $op_links));
  }
  return count($rows) ? theme('table', $header, $rows) : t('No stats page found. <a href="!url">Click here to add</a>', array('!url' => url('admin/content/stats-gen/add')));
}

/**
 * Loads the statistics details.
 */ 
function statistics_generator_load_stats($sgid = NULL, $load_items = TRUE) {
  $stats = array();
  $sql = 'SELECT * FROM {statistics_generator}';
  if (!is_null($sgid)) {
    $sql .= ' WHERE sgid = %d';
  }
  $result = db_query($sql, $sgid);
  while ($row = db_fetch_array($result)) {
    if ($load_items) {
      $row['items'] = statistics_generator_load_stats_items($sgid);
    }
    $stats[$row['sgid']] = $row;    
  }  
  return is_null($sgid) ? $stats : $stats[$sgid];
}

/**  
 * Retrieve the statistics generator group by machine name 
 *  
 * @param $name
 *   machine name for the group
 * @return array
 */
function statistics_generator_load_by_name($name = NULL, $load_items = TRUE) {
  $stats = array();
  if (!$name) {
    return $stats;
  }

  $result = db_query("SELECT * FROM {statistics_generator} WHERE machine_name = '%s'", $name);
  $stats = db_fetch_array($result);

  if ($load_items) {
    $stats['items'] = statistics_generator_load_stats_items($stats['sgid'], FALSE);
  }

  return $stats;
}

/**
 * Loads the views items associated to statistics.
 */ 
function statistics_generator_load_stats_items($sgid, $as_hash = TRUE) {
  $items = array();
  $result = db_query('SELECT * FROM {statistics_generator_items} WHERE stat_id = %d ORDER BY position', $sgid);
  while ($row = db_fetch_array($result)) {
    if ($as_hash) {
      $items[$row['item_id']] = $row;
    }
    else {
      $items[] = $row;
    }
  }
  return $items;
}

/**
 * Implementation of hook_theme()
 */
function statistics_generator_theme() {  
  return array(
    'statistics_generator_stats_form' => array(
      'arguments' => array('form' => NULL),
      'template' => 'statistics-generator-stats-form',      
      'path' => drupal_get_path('module', 'statistics_generator'),
    ),
    'statistics_generator_compare_panel' => array(
      'arguments' => array('compare_results' => NULL),
      'template' => 'compare_panel',      
    ), 
    'statistics_generator_empty_results' => array(
      'arguments' => array(),
      'template' => 'empty_results',      
    ), 
    'statistics_generator_groups_menu' => array(
      'arguments' => array('stats' => NULL),
      'template' => 'groups_menu',      
    ), 
    'statistics_generator_filter_wrapper' => array(
      'arguments' => array('filter_form' => NULL, 'sgid' => NULL, 'delta' => NULL),
      'template' => 'filter_wrapper',      
    ), 
  );
}

/**
 * Implementation of hook_block().
 */
function statistics_generator_block($op = 'list', $delta = 0) {
  switch ($op) {
    case 'list':
      $blocks['statistics_generator_viewfilters'] = array('info' => t('Views statistics filter'));
      $blocks['statistics_generator_cusfilters'] = array('info' => t("Statistics generator custom filter."));
      $blocks['statistics_generator_menu'] = array('info' => t("Statistics generator groups menu."));
      return $blocks;
    case 'view':
      $block = array();

      if ($delta == 'statistics_generator_viewfilters') {
        $function = '_statistics_generator_block_' . $delta;
        if (function_exists($function)) {
          return call_user_func($function);
        }
      }
      else if ($delta == 'statistics_generator_cusfilters') {
        if (arg(0) == 'statistics-generator' && is_numeric(arg(1)) && arg(2) == 'view') {
          $sgid = arg(1);
          $filter_form = drupal_get_form('_statistics_generator_filter_view', $sgid);
          $block['subject'] = "Statistics custom filter";
          $block['content'] = theme('statistics_generator_filter_wrapper', $filter_form, $sgid, $delta);
          return $block;
        }
      }
      else if ($delta == 'statistics_generator_menu') {
        $block['subject'] = t('Statistics groups menu');
        $block['content'] = statistics_generator_groups_menu();
        return $block;
      }

      break;
  }
}

/**
 * Preprocessor for theme('views_view') to hide all the views exposed filters, this could be added
 * if needed with Views statistics filter block.
 */
function statistics_generator_preprocess_views_view(&$vars) {
  $view = $vars['view'];
  // Whisk away exposed filter for display in a block
  _statistics_generator_block_filters($vars['exposed']);
  $vars['exposed'] = '';
}

/**
 * Filter block.
 */
function _statistics_generator_block_filters($set = '') {
  static $filters;
  if (!empty($set)) {
    $filters = $set;
    return;
  }
  if (!empty($filters)) {
    return array('subject' => t('Filter results'), 'content' => $filters);
  }
  return array();
}


/**
 * Implementation of hook_features_api().
 */
function statistics_generator_features_api() {
  return array(
    'statistics_generator' => array(
      'name' => t('Statistics generator groups'),
      'default_hook' => 'statistics_generator_default_groups',
    )
  );
}

/**
 * Implementation of hook_features_export_options().
 */
function statistics_generator_features_export_options() {
  $options = array();
  foreach (statistics_generator_load_stats() as $group) {
    $options[$group['machine_name']] = $group['title'];
  }
  return $options;
}

/**
 * Implementation of hook_features_export().
 */
function statistics_generator_features_export($data, &$export, $module_name = '') {
  // Collect a module to preset map
  foreach ($data as $group) {
    $export['features']['statistics_generator'][$group] = $group;
  }
}

/**
 * Implementation of hook_features_export_render().
 */
function statistics_generator_features_export_render($module_name, $data) {
  $items = array();
  foreach ($data as $key) {
    $group = statistics_generator_load_by_name($key);
    _statistics_generator_features_group_sanitize($group);
    $items[$key] = $group;
  }
  $code = "  \$items = ". features_var_export($items, '  ') .";\n";
  $code .= '  return $items;';
  return array('statistics_generator_default_groups' => $code);
}

/**
 * Implementation of hook_features_revert().
 */
function statistics_generator_features_revert($module) {
  if ($defaults = features_get_default('statistics_generator', $module)) {

    foreach ($defaults as $object) {
      $sgid = db_result(db_query("SELECT sgid FROM {statistics_generator} WHERE machine_name = '%s'",
            $object['machine_name']));
      if (empty($sgid)) {
        // create the new group, does not exist
        drupal_write_record('statistics_generator', $object);
        $sgid = $object['sgid'];
        foreach ($object['items'] as &$group_item) {
          $group_item['stat_id'] = $sgid;
          drupal_write_record('statistics_generator_items', $group_item);
        }
      }
      else {
        // update the group
        $object['sgid'] = $sgid;
        drupal_write_record('statistics_generator', $object, 'sgid');

        // delete the group items to recreate
        db_query("DELETE FROM {statistics_generator_items} WHERE stat_id = %d", $sgid);
        foreach ($object['items'] as &$group_item) {
          $group_item['stat_id'] = $sgid;
          drupal_write_record('statistics_generator_items', $group_item);
        }
      }
    }
  }

  return TRUE;
}

/**
 * Remove unnecessary keys for export.
 */
function _statistics_generator_features_group_sanitize(&$group) {
  $omit = array('sgid', 'item_id', 'stat_id');
  if (is_array($group)) {
    foreach ($group as $k => $v) {
      if (in_array($k, $omit, TRUE)) {
        unset($group[$k]);
      }
      else if (is_array($v)) {
        _statistics_generator_features_group_sanitize($group[$k]);
      }
    }
  }
}

function statistics_generator_groups_menu() {
  $stats = statistics_generator_load_stats(NULL, FALSE);
  if(user_access("access administration pages")) {
    return theme('statistics_generator_groups_menu', $stats);
  }
}  
