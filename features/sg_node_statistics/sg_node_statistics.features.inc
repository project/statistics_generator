<?php

/**
 * Implementation of hook_statistics_generator_default_groups().
 */
function sg_node_statistics_statistics_generator_default_groups() {
  $items = array(
    'node_publications' => array(
      'title' => 'Node publications',
      'machine_name' => 'node_publications',
      'items' => array(
        '0' => array(
          'item' => 'sg_users_publication',
          'display_id' => 'page_1',
          'compare_display_id' => 'page_1',
          'position' => '1',
          'status' => '1',
        ),
        '1' => array(
          'item' => 'sg_nodes_type_publication',
          'display_id' => 'default',
          'compare_display_id' => 'default',
          'position' => '1',
          'status' => '1',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function sg_node_statistics_views_api() {
  return array(
    'api' => '2',
  );
}
