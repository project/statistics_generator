<?php

/**
 * Implementation of hook_views_default_views().
 */
function sg_node_statistics_views_default_views() {
  $views = array();

  // Exported view: sg_nodes_type_publication
  $view = new view;
  $view->name = 'sg_nodes_type_publication';
  $view->description = 'Statistics of nodes publication by type';
  $view->tag = 'statistics_generator';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Predefinidos', 'default');
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => 'Tipo',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 1,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'name' => array(
      'label' => 'User name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'views_sql_groupedfields' => array(
      'label' => 'Group By Fields',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => '1',
      'id' => 'views_sql_groupedfields',
      'table' => 'views_groupby',
      'field' => 'views_sql_groupedfields',
      'relationship' => 'none',
      'views_groupby_fields_to_group' => array(
        'type' => 'type',
      ),
      'views_groupby_sql_function' => 'count',
      'views_groupby_fields_to_aggregate' => array(
        'nid' => 'nid',
      ),
      'views_groupby_field_sortby' => 'nid',
      'views_groupby_sortby_direction' => 'desc',
    ),
  ));
  $handler->override_option('filters', array(
    'date_filter' => array(
      'operator' => 'between',
      'value' => array(
        'value' => NULL,
        'min' => NULL,
        'max' => NULL,
        'default_date' => '',
        'default_to_date' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'date_filter_op',
        'identifier' => 'date_filter',
        'label' => 'Fecha: Date (node)',
        'optional' => 1,
        'remember' => 0,
      ),
      'date_fields' => array(
        'node.created' => 'node.created',
      ),
      'date_method' => 'OR',
      'granularity' => 'day',
      'form_type' => 'date_select',
      'default_date' => '',
      'default_to_date' => '',
      'year_range' => '-3:+3',
      'id' => 'date_filter',
      'table' => 'node',
      'field' => 'date_filter',
      'relationship' => 'none',
    ),
    'uid' => array(
      'operator' => 'in',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'uid_op',
        'identifier' => 'uid',
        'label' => 'Usuario: Nombre',
        'optional' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'none',
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Published nodes by type');
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'charts');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'views_charts_series_fields' => array(
      'nid' => 'nid',
    ),
    'views_charts_x_labels' => 'type',
    'width' => '400',
    'height' => '400',
    'engine' => 'bluff',
    'type' => array(
      'bluff' => 'side_bar',
    ),
    'wmode' => 'window',
    'y_min' => '1',
    'y_max' => '',
    'y_step' => '',
    'y_legend' => '',
    'background_colour' => '',
    'series_colours' => '#A6CEE3,#1F78B4,#B2DF8A,#33A02C,#F03B20,#E31A1C,#FFFFB2,#FED976,#FEB24C,#FD8D3C,#F03B20,#BD0026',
  ));
  $handler = $view->new_display('page', 'Página', 'page_1');
  $handler->override_option('path', 'statistics/published_nodes_type');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  // Exported view: sg_users_publication
  $view = new view;
  $view->name = 'sg_users_publication';
  $view->description = 'Statistics of users publications';
  $view->tag = 'statistics_generator';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Predefinidos', 'default');
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'User name',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'overwrite_anonymous' => 0,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'none',
    ),
    'views_sql_groupedfields' => array(
      'label' => 'Group By Fields',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'alt' => '',
        'link_class' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => '1',
      'id' => 'views_sql_groupedfields',
      'table' => 'views_groupby',
      'field' => 'views_sql_groupedfields',
      'relationship' => 'none',
      'views_groupby_fields_to_group' => array(
        'name' => 'name',
      ),
      'views_groupby_sql_function' => 'count',
      'views_groupby_fields_to_aggregate' => array(
        'nid' => 'nid',
      ),
      'views_groupby_field_sortby' => 'nid',
      'views_groupby_sortby_direction' => 'desc',
    ),
  ));
  $handler->override_option('filters', array(
    'date_filter' => array(
      'operator' => 'between',
      'value' => array(
        'value' => NULL,
        'min' => NULL,
        'max' => NULL,
        'default_date' => '',
        'default_to_date' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'date_filter_op',
        'identifier' => 'date_filter',
        'label' => 'Fecha: Date (node)',
        'optional' => 1,
        'remember' => 0,
      ),
      'date_fields' => array(
        'node.created' => 'node.created',
      ),
      'date_method' => 'OR',
      'granularity' => 'day',
      'form_type' => 'date_select',
      'default_date' => '',
      'default_to_date' => '',
      'year_range' => '-3:+3',
      'id' => 'date_filter',
      'table' => 'node',
      'field' => 'date_filter',
      'relationship' => 'none',
    ),
    'uid' => array(
      'operator' => 'in',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'uid_op',
        'identifier' => 'uid',
        'label' => 'Usuario: Nombre',
        'optional' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'none',
    ),
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Published nodes by users');
  $handler->override_option('style_plugin', 'charts');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'views_charts_series_fields' => array(
      'nid' => 'nid',
    ),
    'views_charts_x_labels' => 'name',
    'width' => '400',
    'height' => '400',
    'engine' => 'bluff',
    'type' => array(
      'bluff' => 'side_bar',
    ),
    'wmode' => 'window',
    'y_min' => '1',
    'y_max' => '',
    'y_step' => '',
    'y_legend' => '',
    'background_colour' => '',
    'series_colours' => '#A6CEE3,#1F78B4,#B2DF8A,#33A02C,#F03B20,#E31A1C',
  ));
  $handler = $view->new_display('page', 'Página', 'page_1');
  $handler->override_option('path', 'statistics/users_publications');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  return $views;
}
