<?php print drupal_render($form['stat_id']) ?>
<?php print drupal_render($form['title']) ?>
<?php print drupal_render($form['machine_name']) ?>
<?php print drupal_render($form['add_items']) ?>

<table>
  <?php if (is_array($form['items'])): ?>
    <?php foreach ($form['items'] as &$item): ?>
      <?php if (empty($item['name'])): ?>
        <?php continue ?>
      <?php endif; ?>
      <tr>
        <td> <?php print drupal_render($item['position']) ?> </td>
        <td> <?php print drupal_render($item['name']) ?> </td>    
        <td> <?php print drupal_render($item['display_id']) ?> </td>
        <td> <?php print drupal_render($item['compare_display_id']) ?> </td>
        <td> <?php print drupal_render($item['remove']) ?> </td>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
</table>
<?php print drupal_render($form) ?>
