This module helps to create custom groups of views that want to be rendered in
the same page and allow to create custom filters that could easy be mapped to
views exposed filters.

A presentation of multiple views could be done using the views aggregation, but
the main problem that inspired me to create this module is the need to create
multiple filters that must be mapped to distinct types of filters.

As an example, imagine a multiple views with a date exposed filter that is not
matching the same date field. We have date filters that points to node creation
date another to the users registration date and another that point to the
accesslog date of visit to the page. Then we have tree types of date filters:
(node, users and accesslog). With the aggregation of views we can share filters
that are common to all the views, but in this case all of them are distinct.

This module resolve that problem implementing a hook when we can declare the
mapping of custom filters to the corresponding views exposed filters. Then we
can create a group of views to what share our custom filters and declare the
callback functions responsible to generate the form item filters.

Hook statistics_generator_filters
-----------------

function hook_statistics_generator_filters($stat) {
  // name of the views that will use the declared filters
  $show_general_filters = array(
    'distributor_statistics', 'products_services_statistics', 'test_statistics'
  );

  if(in_array($stat['machine_name'], $show_general_filters)) {
    $filters = array();
    $filters[] = array(
        'callback' => '_statistics_generator_filter_taxonomy',
        'args' => array(
          'vid' => 6,
          'title' => t('Filter by localization'),
          'labels' => array(
            t('Select a province'),
            t('Select a city'),
          ),
        ),
        'apply_view_filters' => array(
          '*_term_node_tid_depth',
          ),
        );

    $filters[] = array(
        'callback' => '_statistics_generator_filter_date',
        'apply_view_filters' => array(
          '*_date_filter',
          ),
        );

  }

  return $filters;
}

The hook_statistics_generator_filters receive the $stat param that correspond to the
group of views that we build and for that will apply the custom filters. We
define a filter array with the function callbacks that are responsible to
generate the filter custom forms. The description of the params are as follow:

- callback -> the name of the function responsible to generate the form of that
  filter. Currently we have 2 types of generic functions to create taxonomy and
  date filters. 
    _statistics_generator_filter_taxonomy is a function that depends on hierarchy_select
    module, very useful to create taxonomy filters, it receive some args to
    cutomize the filter widget:
    
    vid = the id of vocabulary, title = title of filter, labels = label for
    each level of the filter that will be pushed in the select inputw.

    _statistics_generator_filter_date is a function that depends on Date module very
    useful to build date filters.

- args -> arguments to pass to the filter form functions as key => value array

- apply_view_filters -> the rule of mapping for the values passed by our custom
  filters. This mapping is build in two ways with the patterns:
  1. name of the view + _ + name of the views filter (system name)
      my_view_date_filter -> view with name (my_view) and filter (date_filter)
  2. delta * + _ + name of the views filter.
      *_date_filter -> all the views (* delta) and filter (date_filter)

The custom filter arguments will be passed to the views corresponding filters
based on the mapping array.

