<ul>
  <?php echo t('Statistics'); ?>
  <?php foreach($stats as $stat): ?>
  <li>
  <?php echo l(t($stat['title']), 'statistics-generator/' . $stat['sgid'] . '/view'); ?>
  </li>
  <?php endforeach; ?>
</ul>
